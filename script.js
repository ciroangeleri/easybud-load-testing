import http from "k6/http";
import { check } from "k6";

// Code outside of it is called "init code", and is run only once per VU.

export let options = {
  vus: 10,
  duration: "10s"
};

export default function() {

  // Code inside default is called "VU code", and is run over and over for as long as the test is running. 
  // VU code can make HTTP requests, emit metrics, and generally do everything you'd expect a load test to do - with a few important exceptions: 
  // You can't load anything from your local filesystem, or import any other modules. This all has to be done from init-code.

  let res = http.get("https://www.google.com/");

  // Checks are like asserts but differ in that they don't halt the execution, 
  // instead, they just store the result of the check, ass or fail, and let the script execution continue. 
  check(res, {
    "success": (r) => r.status == 200,
    "body size is 1176 bytes": r => r.body.length == 1176
  });
};


// For more info on K6 check out https://k6.io/docs/using-k6/http-requests it's awesome!