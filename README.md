# EasyBud Load Testing Repo #

This repo is where all the load/performance/stress script tests of the EasyBud will be.

## Guide

* Installation
* Running tests
* Viewing results
* Useful links 👍

## Installation

### K6

K6 is our preffered test runner. It easy, simple and you can write your tests on NodeJS!

MacOS 
```
$ brew tap loadimpact/k6
$ brew install k6
```

Linux (Debian/Ubuntu)
```
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 379CE192D401AB61
$ echo "deb https://dl.bintray.com/loadimpact/deb stable main" | sudo tee -a /etc/apt/sources.list
$ sudo apt-get update
$ sudo apt-get install k6
```

### InfluxDB and Grafana

Want some graphs? It's simpler than you think, using InfluxDB for data storage and Grafana for visualization.

![Grafana Graph](https://k6.io/docs/static/5b7ecf17cae56371683a7ecdce447a4b/ce447/grafana-visualization.png)

MacOS 
```
$ brew install influxdb
$ brew install grafana
```

Linux (Debian/Ubuntu)
```
$ sudo apt install influxdb
$ sudo apt install grafana
```

NOTE: By default, InfluxDB server runs on port 8086 and Grafana server runs on port 3000.


## Running tests

Start InfluxDB to store the results in a separate console

```
$ influxd 
```

Start Graphana so you can view the results later 
```
brew services start grafana
```

To run the tests:
```
$ k6 run --out influxdb=http://localhost:8086/resultsdb script.js
```
Where 

```resultsdb``` -> your database to be created in InfluxDB.

```script.js``` -> the script test you want to run.

This will output something like this:
```


          /\      |‾‾|  /‾‾/  /‾/   
     /\  /  \     |  |_/  /  / /    
    /  \/    \    |      |  /  ‾‾\  
   /          \   |  |‾\  \ | (_) | 
  / __________ \  |__|  \__\ \___/ .io

  execution: local
     output: influxdb=http://localhost:8086/resultsdb (http://localhost:8086)
     script: script.js

    duration: 10s, iterations: -
         vus: 10,  max: 10

    done [==========================================================] 10s / 10s

    ✓ success

    checks.....................: 100.00% ✓ 1099 ✗ 0   
    data_received..............: 14 MB   1.4 MB/s
    data_sent..................: 165 kB  16 kB/s
    http_req_blocked...........: avg=2.02ms   min=0s      med=0s      max=222.82ms p(90)=1µs      p(95)=1µs     
    http_req_connecting........: avg=180.42µs min=0s      med=0s      max=22.6ms   p(90)=0s       p(95)=0s      
    http_req_duration..........: avg=88.36ms  min=66.01ms med=84.05ms max=328.2ms  p(90)=102.56ms p(95)=112.48ms
    http_req_receiving.........: avg=6.95ms   min=87µs    med=5.01ms  max=220.21ms p(90)=12.43ms  p(95)=15.77ms 
    http_req_sending...........: avg=28.36µs  min=14µs    med=26µs    max=135µs    p(90)=40µs     p(95)=45µs    
    http_req_tls_handshaking...: avg=1.57ms   min=0s      med=0s      max=175.2ms  p(90)=0s       p(95)=0s      
    http_req_waiting...........: avg=81.38ms  min=60.59ms med=77.47ms max=271.79ms p(90)=95.74ms  p(95)=104.65ms
    http_reqs..................: 1099    109.899623/s
    iteration_duration.........: avg=90.52ms  min=66.14ms med=84.22ms max=334.11ms p(90)=103.9ms  p(95)=119.11ms
    iterations.................: 1099    109.899623/s
    vus........................: 10      min=10 max=10
    vus_max....................: 10      min=10 max=10
```




## Viewing results

### Creating a Data Source

Now head over to http://localhost:3000. Then create a data source.

- If it asks for a DB select InfluxDB
- Add a name for your datasource 
- Under "InfluxDB Details" add the database details (username, name)

Check if your connection work by clicking "Save and Test" you should see the following:

![Data Source](https://i.imgur.com/NbW8DWL.png)

### Creating a Dashboard

In graphana you can create your own dashboard with your own graphs.

Once you have created a Data Source, go to the "+" to create a Dashboard.

For simplicity sake you can click on Import and type in the dashboard id. To import [this](https://grafana.com/grafana/dashboards/2587) dashboard type 2587.

Then click on Load and you are ready to go!


## Useful links
 - Grafana Docs: https://grafana.com/docs/grafana/latest/
 - InfluxDB Docs: https://v2.docs.influxdata.com/v2.0/get-started/
 - K6 Docs: https://k6.io/docs/

### Who do I talk to? ###

* Ciro Angeleri